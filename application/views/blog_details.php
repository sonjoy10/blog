<!-- content -->
<div class="wrapper">
    <div class="content">
        <?php
        foreach ($all_blog as $v_blog) {
            ?>
            <div class="post-item post-detail">
                <div class="date-box">
                    <span class="post-date"><?php echo $v_blog->date; ?></span>
                    <span class="post-icon"><img src="images/icons/icon_cat_quotes.png" width="30" height="30" alt="" /></span>
                </div>

                <h1><?php echo $v_blog->blog_title; ?></h1>

                <div class="post-image"><img src="<?php echo $v_blog->blog_image; ?>" width="580" height="349" alt="" /></div>

                <div class="post-meta-top">
                    <div class="alignleft">Posted by <span class="author"><a href="#"><?php echo $v_blog->author_name; ?></a></span></div>
                    <img src="images/temp/social_share.gif" width="191" height="20" alt="" />
                </div>

                <div class="entry">
                    <p><?php echo $v_blog->blog_long_description; ?></p>


                    <div class="clear"></div>
                </div>

                <!--                <div class="post-tags">
                                    <span>Tags:</span> <a href="#">Packaging Design</a>,  <a href="#">Portfolios</a>,  <a href="#">Inspiration</a>, <a href="#">Images</a>
                                </div>-->

            </div>


            <!-- post comments -->
            <div class="comment-list" id="comments">

                <h2><?php echo count($all_comment); ?> Comments</h2>

                <ol>
                    <?php
                    foreach ($all_comment as $v_comment) {
                        ?>  
                        <li class="comment">
                            <div class="comment-body">

                                <div class="avatar"><img src="images/temp/gavatar_1.jpg" width="48" height="48" alt="" /></div>
                                <div class="comment-text">
                                    <div class="comment-author"><a href="#" class="link-author"><?php echo $v_comment->name; ?></a> <span class="comment-date"><?php echo $v_comment->datetime; ?></span></div>
                                          <div class="comment-entry"><?php echo $v_comment->message; ?><a href="#addcomment" class="link-reply">reply</a></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                </ol>
            </div>

            <!--/ post comments -->

            <!-- add comment -->
            <div class="add-comment" id="addcomments">
                <h3>Add a comment</h3>

                <div class="comment-form">
                    <form action="<?php echo base_url(); ?>welcome/save_comments" method="post">

                        <div class="row  alignleft">
                            <label>Your email (required)</label>
                            <input type="hidden" name="blog_id" value="<?php echo $v_blog->blog_id; ?>" />
                            <input type="text" name="email" value="" class="inputtext input_middle required" />
                        </div>

                        <div class="row alignleft">
                            <label>Your name (optional)</label>
                            <input type="text" name="name" value="" class="inputtext input_middle required" />
                        </div>

                        <div class="clear"></div>   

                        <div class="row">
                            <label>Your message</label>
                            <textarea cols="30" rows="10" name="message" class="textarea textarea_middle required"></textarea>
                        </div>

                        <div class="row">
                            <input type="submit" value="Submit Message" class="btn-submit" />
                        </div>
                    </form>
                </div>
            </div>
            <!--/add comment -->   
            <?php
        }
        ?>

        <div class="clear"></div>


    </div>
</div>
<!-- content -->
<div class="wrapper">
    <div class="content">
        <?php
        foreach ($all_blog as $v_blog) {
            ?>
            <div class="post-item">
                <div class="date-box">
                    <span class="post-date"><strong><?php echo $v_blog->date; ?></span>
                    <span class="post-icon"><img src="<?php echo base_url(); ?>source/images/icons/icon_cat_photo.png" width="30" height="30" alt="" /></span>
                </div>

                <h2><a href="blog-details.html"><?php echo $v_blog->blog_title; ?></a></h2>

                <div class="post-image"><img src="<?php echo $v_blog->blog_image; ?>" width="580" height="349" alt="" /></div>

                <div class="post-meta-top">
                    <div class="alignleft">Posted by : <span class="author"><a href="#"><?php echo $v_blog->author_name; ?></a></span></div>
                    <img src="<?php echo base_url(); ?>source/images/temp/social_share.gif" width="191" height="20" alt="" />
                </div>

                <div class="entry">
                    <p><?php echo $v_blog->blog_short_description; ?></p> 
                    <div class="clear"></div>
                </div>

                <!--                    <div class="post-tags">
                                                                <span>Tags:</span> <a href="#">Packaging Design</a>,  <a href="#">Portfolios</a>,  <a href="#">Inspiration</a>, <a href="#">Images</a>
                                                        </div>-->

                <div class="post-meta-bot">
                    <div class="alignleft"><a href="<?php echo base_url(); ?>welcome/details/<?php echo $v_blog->blog_id; ?>" class="link-more">CONTINUE READING &gt;</a></div>
                    <a href="#" class="link-comments">14 comments</a>
                </div>
            </div>

            <?php
        }
        ?>



        <div class="clear"></div>

        

    </div>
    <div class="tf_pagination">
    <a class="page_prev" href="#">PREV</a> 
    <a href="#">1</a> <a href="#">2</a> <a href="#">3</a> <a href="#" class="page_current">4</a> <a href="#">5</a> <a href="#">6</a> ... <a href="#">17</a> 
    <a class="page_next" href="#">NEXT</a>
</div>
</div>
<!--/ content -->

<!-- sidebar -->
<!-- pagination -->

<!--/ pagination -->



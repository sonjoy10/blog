<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- Mirrored from demo.themefuse.com/html/Writer/index-style-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Dec 2015 19:14:37 GMT -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $title; ?></title>
        <meta name="author" content="ThemeFuse" />
        <meta name="description" content="A short description of your company" />
        <meta name="keywords" content="Some keywords that best describe your business" />

        <link href="<?php echo base_url(); ?>source/css/style.css" media="screen" rel="stylesheet" type="text/css" />
        
        <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'/>

        <!--[if lte IE 7]>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>source/css/ie.css" />
        <![endif]-->
        <!--[if IE 9 ]><link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>source/css/ie9.css" /><![endif]--> 

        <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>source/js/jquery.min.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>source/js/jquery.tools.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>source/js/preloadCssImages.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>source/js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>source/js/general.js"></script>

        <link href="<?php echo base_url(); ?>source/css/custom.css" media="screen" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <div class="bodywrap skin_grass">

            <div class="header">
                <div class="container">
                    <div class="header_menu">

                        <div class="logo"><a href="index.html"><img src="<?php  echo base_url(); ?>source/images/logo.png" alt="Blog Writer" width="207" height="113" border="0" /></a></div>
                        <!--<div class="logo"><h1><a href="index.html">KHOBOR</a></h1></div>-->

                        <div class="topmenu tmenu_left">
                            <ul class="dropdown">
                                <li class="current-menu-ancestor menu-item-home"><a href="<?php echo base_url(); ?>welcome"><span>Home</span></a>

                                </li>
                                <li><a href="about.html"><span>About</span></a></li>
                                <li class=""><a href="#"><span>Category</span></a>
                                    <ul>
                                        <?php
                                        foreach ($all_category as $v_category) {
                                            ?>
        <!--                                        <li class="current-menu-item"><a href="index-style-1.html"><span>Header Skin: Grass</span></a></li>-->
                                            <li><a href="index-style-2.html"><span><?php echo $v_category->category_name; ?></span></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>

                        <div class="topmenu tmenu_right">
                            <ul class="dropdown">
                                <li><a href="portfolio.html"><span>Work</span></a>
                                    <ul>
                                        <li><a href="portfolio.html"><span>Full Width Gallery</span></a></li>
                                        <li><a href="portfolio-2cols.html"><span>Gallery with Sidebar</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="#"><span>Shortcodes</span></a>
                                    <ul>
                                        <li><a href="shortcodes-text.html"><span>Text and Images</span></a></li>							
                                        <li><a href="shortcodes-buttons.html"><span>Buttons &amp; Lists</span></a></li> 
                                        <li><a href="shortcodes-charts.html"><span>Charts</span></a></li>  
                                        <li><a href="shortcodes-columns.html"><span>Columns</span></a>
                                            <ul>
                                                <li><a href="#"><span>One Column</span></a></li>
                                                <li><a href="#"><span>Two Columns</span></a></li>
                                                <li><a href="#"><span>Three Columns</span></a></li> 
                                                <li><a href="#"><span>Full width</span></a></li> 
                                            </ul>
                                        </li>  
                                        <li><a href="shortcodes-lightbox.html"><span>Lightbox</span></a></li>
                                        <li><a href="shortcodes-media.html"><span>Media</span></a></li>
                                        <li><a href="shortcodes-tables.html"><span>Tables</span></a></li>
                                        <li><a href="shortcodes-tabs.html"><span>Tabs and Toggles</span></a></li>
                                        <li><a href="shortcodes-typography.html"><span>Typography</span></a></li>
                                        <li><a href="shortcodes-widgets.html"><span>Widgets</span></a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"><span>Contact</span></a></li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <!--/ header menu -->

                    <div class="header_title_block">
                        <div class="header_title">
                            <h1>Welcome to Our <span>Majar Goli</span>. Enjoy!</h1>
                        </div>

                        <div class="header_social">                           
                            <a href="#" class="top_icon_twitter">Twitter</a>
                            <a href="#" class="top_icon_facebook">Facebook</a>                           
                              
                            <?php
                            $blogger_id = $this->session->userdata('blogger_id');
                            $blogger_name = $this->session->userdata('blogger_name');
                            
                            if ($blogger_id) {
                                ?>
                                <a href="<?php echo base_url(); ?>welcome" class="top_icon_signup" ><?php echo $blogger_name; ?></a>
                                 <a href="<?php echo base_url(); ?>blogger/blogger_logout" class="top_icon_login" >Logout</a>                               
                                
                                <?php
                            } else {
                                
                                ?>
                               <a href="<?php echo base_url(); ?>blogger" class="top_icon_signup" >Sign Up</a> 
                               <a href="<?php echo base_url(); ?>blogger/login" class="top_icon_login" >Login</a>                           
                                <?php
                            }
                            ?>
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>
            <!--/ header -->
            <div class="middle" id="sidebar_right">
                <div class="container">
                    <div class="outerContent">
                        <div class="innerContent">

                            <?php echo $mainContent; ?>

                            <!--/ middle -->
                            <div class="sidebar">

                                <div id="search-2" class="widget-container widget_search">
                                    <form method="get" id="yolink_searchform" action="http://themefuse.com/test/htmldev/" >
                                        <div>
                                            <label class="screen-reader-text" for="s">Search for:</label>
                                            <input type="text" name="s" id="s" value="Search the Blog" onfocus="if (this.value == 'Search the Blog') {
                                                        this.value = '';
                                                    }" onblur="if (this.value == '') {
                                                                this.value = 'Search the Blog';
                                                            }" />
                                            <input type="submit" id="searchsubmit" value="Search" />

                                        </div>
                                    </form>
                                    <div class="clear"></div>
                                </div>

                                <div id="text-1" class="widget-container widget_text">
                                    <h3 class="widget-title">About Me</h3>
                                    <div class="textwidget">
                                        <img src="<?php echo base_url(); ?>source/images/temp/author_image_1.jpg" width="98" height="98" alt="" class="alignleft" /> Hello, welcome to my weblog! My name is <strong>Marc Seringhetti</strong> and i'm a student at UCLA and i love cool stuff &amp; risus eu commodo interdum, nunc ipsum mollis purpharetra eget leo.
                                    </div>
                                </div>	

                                <div class="widget-container widget_recent_entries">
                                    <h3 class="widget-title">Recent Posts</h3>
                                    <ul>
                                        <li><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_1.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a> <a href="blog-details.html">Lovely &amp; Creative Packaging</a></li>
                                        <li><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_2.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a> <a href="blog-details.html">Video: Crazy Days of Summer</a></li>
                                        <li><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_3.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a> <a href="blog-details.html">Audio: new album by Foo Fighters lorem ipsum esta</a></li>
                                        <li><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_4.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a> <a href="blog-details.html">Normal post containing text</a></li>
                                        <li><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_5.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a> <a href="blog-details.html">Link to a great resource</a></li>
                                    </ul>
                                </div>


                                <div class="widget-container widget_recent_comments">
                                    <h3 class="widget-title">Recent Comments</h3>
                                    <ul id="recentcomments">
                                        <li class="recentcomments"><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_2.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a>  <a href="http://themefuse.com/test/" rel="external nofollow" class="url">Mr WordPress</a> on <a href="http://themefuse.com/test/htmldev/2011/05/10/hello-world/#comment-1">Hong Kong Phooey, number one super guy, he is quicker than the human eye</a></li>
                                        <li class="recentcomments"><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_1.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a>  Julia Styles on <a href="http://themefuse.com/test/htmldev/2011/05/10/hello-world/#comment-1">Down the road, that's where I'll always be</a></li>
                                        <li class="recentcomments"><a href="blog-details.html"><img src="<?php echo base_url(); ?>source/images/temp/post_thumb_3.jpg" alt="" class="thumbnail" border="0" height="30" width="30" /></a>  <a href="http://themefuse.com/test/" rel="external nofollow" class="url">Serj Bagrin</a> on <a href="http://themefuse.com/test/htmldev/2011/05/10/hello-world/#comment-1">Maybe tomorrow I'll want to settle down!</a></li>
                                    </ul>
                                </div>

                                <!-- sidebar 2 columns -->
                                <div class="sidebar_col_1">
                                    <div id="archives-3" class="widget-container widget_archive">
                                        <h3 class="widget-title">Archives</h3>		
                                        <ul>
                                            <li><a href='#' title='June 2011'>June 2011</a></li>
                                            <li><a href='#' title='June 2011'>June 2011</a></li>
                                            <li><a href='#' title='May 2011'>May 2011</a></li>
                                            <li><a href='#' title='April 2011'>April 2011</a></li>
                                            <li><a href='#' title='March  2011'>March 2011</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="sidebar_col_2">
                                    <div id="archives-4" class="widget-container widget_socials">
                                        <h3 class="widget-title">FOLLOW ME</h3>		
                                        <ul>
                                            <li><a href="#"><img src="<?php echo base_url(); ?>source/images/icons/social_facebook_24.png" width="24" height="24" alt="" /> Facebook</a></li>
                                            <li><a href="#"><img src="<?php echo base_url(); ?>source/images/icons/social_flickr_24.png" width="24" height="24" alt="" /> Flickr</a></li>
                                            <li><a href="#"><img src="<?php echo base_url(); ?>source/images/icons/social_twitter_24.png" width="24" height="24" alt="" /> Twitter</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <!--/ sidebar 2 columns -->





                                <div class="widget-container widget_twitter">
                                    <h3>Latest Tweets</h3>   

                                    <div class="tweet_item">
                                        <div class="tweet_image"><img src="<?php echo base_url(); ?>source/images/temp/twitter_avatar.png" width="30" height="30" alt="" /></div>
                                        <div class="tweet_text">
                                            <div class="inner">
                                                <a href="#">@LesPaul:</a> the guitar's probably worth at least $25k. Interested?
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>   

                                    <div class="tweet_item">
                                        <div class="tweet_image"><img src="<?php echo base_url(); ?>source/images/temp/twitter_avatar_2.jpg" width="30" height="30" alt="" /></div>
                                        <div class="tweet_text">
                                            <div class="inner">
                                                <a href="#">@Serendipity:</a> knowing you, every bit of time spent on that car is like a vacation in paradise ;)
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="tweet_item">
                                        <div class="tweet_image"><img src="<?php echo base_url(); ?>source/images/temp/twitter_avatar.png" width="30" height="30" alt="" /></div>
                                        <div class="tweet_text">
                                            <div class="inner">
                                                <a href="#">@LesPaul:</a> great news!
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                </div>  

                                <div class="widget-container widget_flickr">
                                    <h3 class="widget-title">IMAGE GALLERY</h3>
                                    <div class="flickr">

                                        <div class="flickr_badge_image" id="flickr_badge_image1"><a href="http://www.flickr.com/photos/themefuse/4745967918/"><img src="../../../farm5.static.flickr.com/4122/4745967918_6780fd65ff_s.jpg" alt="A photo on Flickr" title="Bon Apetit HTML/WP template - Homepage" height="75" width="75" /></a></div>
                                        <div class="flickr_badge_image" id="flickr_badge_image2"><a href="http://www.flickr.com/photos/themefuse/4745967666/"><img src="../../../farm5.static.flickr.com/4119/4745967666_b996719848_s.jpg" alt="A photo on Flickr" title="TechOffers HTML Newsletter" height="75" width="75" /></a></div>
                                        <div class="flickr_badge_image" id="flickr_badge_image3"><a href="http://www.flickr.com/photos/themefuse/4745329921/"><img src="../../../farm5.static.flickr.com/4100/4745329921_23b2c3eb01_s.jpg" alt="A photo on Flickr" title="SiliconApp HTML/WP template - Support" height="75" width="75" /></a></div>
                                        <div class="flickr_badge_image" id="flickr_badge_image4"><a href="http://www.flickr.com/photos/themefuse/4745970022/"><img src="../../../farm5.static.flickr.com/4095/4745970022_cb514a9909_s.jpg" alt="A photo on Flickr" title="Art Gallery HTML/WP template - Homepage" height="75" width="75" /></a></div>
                                        <span style="position: absolute; left: -999em; top: -999em; visibility: hidden;" class="flickr_badge_beacon"><img src="http://geo.yahoo.com/p?s=792600102&amp;t=a268516684fee260dc5cde12b3bc2bba&amp;r=http%3A%2F%2Fthemefuse.com%2Fdemo%2Fwp%2Fqlassik%2F%3Fpage_id%3D425&amp;fl_ev=0&amp;lang=en&amp;intl=us" alt="" height="0" width="0" /></span> 
                                        <div class="clear"></div>
                                    </div>
                                </div>   





                            </div>
                            <!--/ sidebar -->

                            <div class="clear"></div>


                        </div>    
                    </div>
                </div>
            </div>

            <div class="footer footer_grass_bg">
                <div class="inner">


                    <div class="container">

                        <div class="f_col_1">

                            <div id="categories-3" class="widget-container widget_categories">
                                <h3 class="widget-title">Categories</h3>
                                <ul>
                                    <li class="cat-item cat-item-1"><a href="#" title="View all posts filed under Lifestyle talk">Lifestyle talk</a></li>
                                    <li class="cat-item cat-item-2"><a href="#" title="View all posts filed under Sports">Sports</a></li>
                                    <li class="cat-item cat-item-3"><a href="#" title="View all posts filed under Classic Cars">Classic Cars</a></li>
                                    <li class="cat-item cat-item-4"><a href="#" title="View all posts filed under Food &amp; Recipes">Food &amp; Recipes</a></li>
                                    <li class="cat-item cat-item-5"><a href="#" title="View all posts filed under Holidays">Holidays</a></li>
                                </ul>
                            </div>

                        </div>
                        <!--/ footer col 1 -->

                        <div class="f_col_2">

                            <div class="widget-container widget_pages">
                                <h3 class="widget-title">LATEST WORK</h3>
                                <ul>
                                    <li><a href="portfolio-details.html">Mobility App</a></li>
                                    <li><a href="portfolio-details.html">Qlassik ID</a></li>
                                    <li><a href="portfolio-details.html">Freshfolio</a></li>
                                    <li><a href="portfolio-details.html">BrandCrafters</a></li>
                                    <li><a href="portfolio-details.html">Clean Classy Cor...</a></li>
                                </ul>
                            </div>

                        </div>
                        <!--/ footer col 2 -->

                        <div class="f_col_3">

                            <div class="widget-container widget_twitter">
                                <h3>From Twitter</h3>   

                                <div class="tweet_item">
                                    <div class="tweet_image"><img src="<?php echo base_url(); ?>source/images/temp/twitter_avatar.png" width="30" height="30" alt="" /></div>
                                    <div class="tweet_text">
                                        <div class="inner">
                                            <a href="#">@LesPaul:</a> the guitar's probably worth at least $25k. Interested?
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>   

                                <div class="tweet_item">
                                    <div class="tweet_image"><img src="<?php echo base_url(); ?>source/images/temp/twitter_avatar.png" width="30" height="30" alt="" /></div>
                                    <div class="tweet_text">
                                        <div class="inner">
                                            How Wade let Taj Gibson just bang on him like that?  <a href="#">http://t.co/LXrOw81</a> No first date or anything... </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>

                            </div>            

                        </div>        
                        <!--/ footer col 3 -->

                        <div class="f_col_4">



                            <!-- widget contacts -->
                            <div class="widget-container widget_contact">                    
                                <h3>Contact Me</h3>     

                                <div class="contact-address">
                                    <div class="address">589 Roadhill Boulevard<br /> Venice Beach, California 90291, USA</div>
                                    <div class="phone">Phone: <strong>+33 (0) 9399 7987</strong></div>
                                    <div class="fax">Fax: <strong>+33 (0) 9399 7988</strong></div>
                                </div>                            
                            </div>
                            <!--/ widget contacts -->           


                            <div class="footer_social">
                                <a href="#"><img src="<?php echo base_url(); ?>source/images/icons/social_facebook_16.png" alt="" width="16" height="16" border="0" /></a>
                                <a href="#"><img src="<?php echo base_url(); ?>source/images/icons/social_twitter_16.png" alt="" width="16" height="16" border="0" /></a>
                                <a href="#"><img src="<?php echo base_url(); ?>source/images/icons/social_master_16.png" alt="" width="16" height="16" border="0" /></a>
                                <a href="#"><img src="<?php echo base_url(); ?>source/images/icons/social_rss_16.png" alt="" width="16" height="16" border="0" /></a>
                                <div class="clear"></div>
                            </div>

                            <div class="copyright"><a href="http://themefuse.com/">Themefuse.com</a>  2011. All rights reserved.</div>

                        </div>
                        <!--/ footer col 4 -->

                        <div class="clear"></div>
                    </div>


                </div> 
            </div>

        </div>
    </body>

    <!-- Mirrored from demo.themefuse.com/html/Writer/index-style-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Dec 2015 19:14:37 GMT -->
</html>


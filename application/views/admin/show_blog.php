<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading" data-original-title>
                <h2><i class="fa fa-user"></i><span class="break"></span>Members</h2>
                <div class="panel-actions">
                    <a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Blog Title</th>
                            <th>Blog Short Description</th>
                            <th>Blog Long Description</th>
                            <th>Author Name</th>
                            <th>Author Email</th>
<!--                            <th>Image Name</th>-->
                            <th>Date</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                                foreach ($all_blog as $v_blog) {
                                    
                                
                            ?>
                        <tr>
                            
                            <td><?php echo $v_blog->blog_id;?></td>
                            <td><?php echo $v_blog->blog_title;?></td>
                            <td><?php echo $v_blog->blog_short_description;?></td>
                            <td><?php echo $v_blog->blog_long_description;?></td>
                            <td><?php echo $v_blog->author_name;?></td>
                            <td><?php echo $v_blog->author_email;?></td>
<!--                            <td ><img style="width:200px; height: 200px" src="<?php echo $v_blog->blog_image;?>" alt="<?php echo $v_blog->blog_image;?>"/></td>-->
                            <td><?php echo $v_blog->date;?></td>
                            <td>
                                <?php
                                    if($v_blog->publication_status==1){
                                ?>
                                <span class="label label-success">Published</span>
                                <?php
                                    }else{
                                ?>
                                <span class="label label-warning">Unpublished</span>
                                <?php
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    if($v_blog->publication_status==1){
                                ?>
                                <a style="text-decoration:none" href="<?php echo base_url();?>super_admin/Unpublish_blog/<?php echo $v_blog->blog_id;?>">
                                    <span class="label label-warning">Unpublished</span>  
                                </a>
                                <?php
                                    }else{
                                ?>
                                <a style="text-decoration:none" href="<?php echo base_url();?>super_admin/publish_blog/<?php echo $v_blog->blog_id;?>">
                                    <span class="label label-success">Published</span>  
                                </a>
                                <?php
                                    }
                                ?>
                                <a class="btn btn-info" href="<?php echo base_url();?>super_admin/edit_blog/<?php echo $v_blog->blog_id;?>"">
                                    <i class="fa fa-edit "></i>  
                                </a>
                                <a onclick="return check();"class="btn btn-danger" href="<?php echo base_url();?>super_admin/delete_blog_by_blog_id/<?php echo $v_blog->blog_id;?>">
                                    <i class="fa fa-trash-o "></i> 
                                </a>
                            </td>
                            
                        </tr>
                        <?php
                                }
                            ?>
                                              
                    </tbody>
                </table>            
            </div>
        </div>
    </div>
</div>

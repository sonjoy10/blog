<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><strong>Category</strong> Elements</h2>
            </div>
            <div class="panel-body">
                
                <form action="<?php echo base_url(); ?>super_admin/update_category/" method="post" enctype="multipart/form-data" class="form-horizontal ">
                    <?php
                $success = $this->session->userdata('success');
                if ($success) {
                    ?>
                    <div class="alert alert-success">                    
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <h3><strong>Well done! </strong><?php echo $success; ?></h3>                       
                    </div>
                    <?php
                }
                $this->session->unset_userdata('success');
                ?>
                    <?php
                    foreach ($all_category as $v_category) {
                        ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="text-input">Category Name</label>
                            <div class="col-md-9">
                                
                                <input type="hidden" id="text-input" value="<?php echo $v_category->category_id; ?>" name="category_id" class="form-control" placeholder="Text">
                                <input type="text" id="text-input" value="<?php echo $v_category->category_name; ?>" name="category_name" class="form-control" placeholder="Text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="text-input">Category Description</label>
                            <div class="col-md-9">
                                <textarea type="text" id="text-input" name="category_description" class="form-control" placeholder="Description"><?php echo $v_category->category_description; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Publication Status</label>
                            <div class="col-md-9">
                                <?php
                                if (($v_category->publication_status) == 1) {
                                    ?>
                                    <label class="" for="inline-radio1">
                                        <input type="radio" id="inline-radio1" name="publication_status" value="1" checked> Published
                                    </label>
                                    <label class="" for="inline-radio2">
                                        <input type="radio" id="inline-radio2" name="publication_status" value="0"> Unpublished
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="" for="inline-radio1">
                                        <input type="radio" id="inline-radio1" name="publication_status" value="1"> Published
                                    </label>
                                    <label class="" for="inline-radio2">
                                        <input type="radio" id="inline-radio2" name="publication_status" value="0" checked> Unpublished
                                    </label>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Update</button>
                            <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                        </div>
                        <?php
                    }
                    ?>
                </form>
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2><strong>Blog</strong> Elements</h2>
            </div>
            <div class="panel-body">


                <form action="<?php echo base_url(); ?>super_admin/update_blog" method="post" enctype="multipart/form-data" class="form-horizontal ">
                    <?php
                    $success = $this->session->userdata('success');
                    if ($success) {
                        ?>
                        <div class="alert alert-success">                    
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <h3><strong>Well done! </strong><?php echo $success; ?></h3>                       
                        </div>
                        <?php
                    }
                    $this->session->unset_userdata('success');
                    ?>
                    <?php
                    foreach ($all_blog as $v_blog) {
                        ?>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="text-input">Blog Title</label>
                            <div class="col-md-9">
                                <input type="hidden" value="<?php echo $v_blog->blog_id; ?>"id="text-input" name="blog_id" class="form-control" placeholder="Text">
                                <input type="text" value="<?php echo $v_blog->blog_title; ?>"id="text-input" name="blog_title" class="form-control" placeholder="Text">

                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="select">Category Name</label>
                            <div class="col-md-9">
                                <select id="select" name="category_id" class="form-control" size="1">
                                    <?php
                                    foreach ($all_category as $value) {
                                        ?>
                                        <option value="<?php echo $value->category_id; ?>">
                                            <?php echo $value->category_name; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="textarea-input">Blog Short Description</label>
                            <div class="col-md-9">
                                <textarea id="textarea-input"  name="blog_short_description" rows="9" class="form-control" placeholder="Content.."><?php echo $v_blog->blog_short_description; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="textarea-input">Blog Long Description</label>
                            <div class="col-md-9">
                                <textarea id="textarea-input"  name="blog_long_description" rows="9" class="form-control" placeholder="Content.."><?php echo $v_blog->blog_long_description; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="text-input">Author Name</label>
                            <div class="col-md-9">
                                <input type="text" id="text-input" value="<?php echo $v_blog->author_name; ?>" name="author_name" class="form-control" placeholder="Text">

                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email-input">Author Email</label>
                            <div class="col-md-9">
                                <input type="email" id="email-input" value="<?php echo $v_blog->author_email; ?>" name="author_email" class="form-control" placeholder="Enter Email">
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="file-input">File input</label>
                            <div class="col-md-9">
                                <input type="file" id="file-input" name="blog_image">
                                <img src="<?php echo $v_blog->blog_image; ?>" alt="<?php echo $v_blog->blog_image; ?>"/>
                            </div>
                        </div>

                        <!--                    <div class="form-group">
                                                <label class="col-md-3 control-label" for="date01">Form with Datepicker</label>
                                                <div class="col-md-9 controls">
                                                    <div class="input-group date col-sm-4">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control date-picker" id="date01" data-date-format="mm/dd/yyyy" name="date"/>
                                                    </div>	
                                                </div>
                                            </div>-->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="text-input">Date</label>
                            <div class="col-md-9">
                                <input type="date" value="<?php echo $v_blog->date; ?>" id="text-input" name="date" class="form-control" placeholder="Text">

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Publication Status</label>
                            <div class="col-md-9">
                                <?php
                                if ($v_blog->publication_status == 1) {
                                    ?>
                                    <label class="" for="inline-radio1">
                                        <input type="radio" id="inline-radio1" name="publication_status" value="1" checked> Published
                                    </label>
                                    <label class="" for="inline-radio2">
                                        <input type="radio" id="inline-radio2" name="publication_status" value="0"> Unpublished
                                    </label>
                                    <?php
                                } else {
                                    ?>
                                    <label class="" for="inline-radio1">
                                        <input type="radio" id="inline-radio1" name="publication_status" value="1" > Published
                                    </label>
                                    <label class="" for="inline-radio2">
                                        <input type="radio" id="inline-radio2" name="publication_status" value="0" checked> Unpublished
                                    </label>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Update</button>
                        <button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('welcome_model');
        $this->load->model('general_model');
    }

    public function index() {
        $data = array();
        $data['title'] = "Majar Goli";
        $data['all_category']=$this->welcome_model->selecet_all_published_category();
        $data["all_blog"] = $this->welcome_model->select_all_published_blog();
        $data["mainContent"] = $this->load->view('home_content', $data, true);
        $this->load->view('master_content', $data);
    }

//        public function contact()
//	{       $data=array();
//                $data['title']="Majar Goli";
//                $data["mainContent"]=$this->load->view('master_content',$data,true);
//		$this->load->view('master_content',$data);
//	}

    public function details($blog_id) {
        $data = array();
        $data['title'] = "Majar Goli";        
        $data["all_comment"] = $this->welcome_model->select_all_comments($blog_id);
        //echo '<pre>';                print_r($data);                                exit();
        $data["all_blog"] = $this->welcome_model->select_all_published_blog_by_blog_id($blog_id);
        $data["mainContent"] = $this->load->view('blog_details', $data, true);
        $this->load->view('master_content', $data);
    }
    
    public function save_comments(){
        $data=array();        
        $data['blog_id']=$this->input->post('blog_id');
        $data['email']=$this->input->post('email');
        $data['name']=$this->input->post('name');
        $data['message']=$this->input->post('message');
        $this->general_model->save_info('tbl_comments',$data);
        
        $sdata=array();
        $sdata['messege']="Comments are successfully Added";
        $this->session->set_userdata($sdata);
        redirect('welcome/details/'.$data['blog_id']);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
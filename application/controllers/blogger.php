<?php


class Blogger extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('welcome_model');
        $this->load->model('general_model');
        $this->load->model('blogger_model');
    }
    public function index(){
        $data=array();
        $data['title']="Majar Goli-Sign Up";  
        $data['all_category']=$this->welcome_model->selecet_all_published_category();
        $data["mainContent"] = $this->load->view('sign_up', $data, true);
        $this->load->view('master_content', $data);   
    }
    public function login(){
        $data=array();
        $data['title']="Majar Goli-Login";  
        $data['all_category']=$this->welcome_model->selecet_all_published_category();
        $data["mainContent"] = $this->load->view('blogger_login', $data, true);
        $this->load->view('master_content', $data);
   
    }
    
   
    
    public function save_blogger_info(){
        $data = array();
        //echo'<pre>';print_r($_POST); exit();
        //echo'<pre>';print_r($_FILES); exit();
        $config['upload_path'] = 'source/images/blogger_images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '10000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $error =array();
        $fdata =array();
        $this->load->library('upload');
        $this->upload->initialize($config);
        //echo'<pre>';print_r($config); exit();

        if (!$this->upload->do_upload('blogger_image')) {
            $error = $this->upload->display_errors();
        } else {
            $fdata =  $this->upload->data();
            $data['blogger_image']= $config['upload_path'].$fdata['file_name'];
            //echo'<pre>';print_r($fdata); exit();
            
        }

        $data['first_name'] = $this->input->post('first_name', true);
        $data['last_name'] = $this->input->post('last_name', true);
        $data['email'] = $this->input->post('email', true);
        $data['password'] = $this->input->post('password', true);
        $data['mobile'] = $this->input->post('mobile', true);
        $data['address'] = $this->input->post('address', true);
        $data['city'] = $this->input->post('city', true);
        $data['country'] = $this->input->post('country', true);
        $data['zip'] = $this->input->post('zip', true);
        $data['dob'] = $this->input->post('dob', true);
        $data['gender'] = $this->input->post('gender', true);
        $data['publication_status'] = $this->input->post('publication_status', true);
        //echo'<pre>';        print_r($data);exit();
        $this->general_model->save_info('tbl_blogger', $data);

        $sdata = array();
        $sdata['success'] = "Blog is successfully Inserted!!";
        $this->session->set_userdata($sdata);
        redirect('welcome');
    }
    
    public function bloger_login(){
        $blogger_email=$this->input->post('blogger_email',true);
        $blogger_password=$this->input->post('blogger_password',true);
        
        $result=$this->blogger_model->blogger_check($blogger_email,$blogger_password);
        //echo '<pre>';print_r($result);exit();
        if($result){
            $bdata=array();
            $bdata['blogger_id']=$result->blogger_id;
            $bdata['blogger_name']=$result->first_name;
            $bdata['message']="You are perfectly login!!";
            $this->session->set_userdata($bdata);
            redirect('Welcome');
        }else{
            $sdata=array();
            $bdata['message']="Your email or password is incorrect!!";
            redirect('blogger/login');
        }
    }
    
    public function blogger_logout(){
        $this->session->unset_userdata('blogger_id');
        $ldata = array();
        $ldata['loged_out'] = "You are successfully loged out.";
        $this->session->set_userdata($ldata);
        redirect("welcome");
    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of super_admin
 *
 * @author sonjoy
 */
class Super_Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('super_admin_model');
        $this->load->model('general_model');
        $this->load->helper('ckeditor');
        $this->data['ckeditor']=array(
          'id'=>'ck_editor',
            'path'=>'source/js/ckeditor',
            'config'=>array(
              'toolbar'=>'Full',
               'width'=>'686px',
                'height'=>'400px'
            )
        );
    }

    public function index() {
        $data = array();
        $data['title'] = "Admin Panel";
        $data['content'] = $this->load->view('admin/dashboard', $data, true);
        $this->load->view('admin/master_admin', $data);
    }

    public function add_category() {
        $data = array();
        $data['title'] = "Admin Panel";
        $data['content'] = $this->load->view('admin/add_category', '', true);
        $this->load->view('admin/master_admin', $data);
    }

    public function save_category() {
        $data = array();
        $data['category_name'] = $this->input->post('category_name', true);
        $data['category_description'] = $this->input->post('category_description', true);
        $data['publication_status'] = $this->input->post('publication_status', true);
        //echo'<pre>';        print_r($data);exit();
        $this->general_model->save_info('tbl_category', $data);
        $sdata = array();
        $sdata['success'] = "Category is successfully Inserted!!";
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_category');
    }
    
    public function show_category() {
        $data = array();
        $data['title'] = "Admin Panel-Category";
        $data['all_category'] = $this->super_admin_model->select_all_category();
        $data['content'] = $this->load->view('admin/show_category', $data, true);
        $this->load->view('admin/master_admin',$data);
    }
    
    public function delete_category_by_category_id($category_id){
        $this->super_admin_model->delete_category($category_id);
        redirect('super_admin/show_category');
    }
    
    public function unpublish_category($category_id){
        $this->super_admin_model->unpublished_category_by_category_id($category_id);
        redirect('super_admin/show_category');
    }
    public function publish_category($category_id){
        $this->super_admin_model->published_category_by_category_id($category_id);
        redirect('super_admin/show_category');
    }
    
    public function edit_category($category_id){
        $data = array();
        $data['title'] = "Admin Panel-Edit Category";  
        $data['all_category']=$this->super_admin_model->edit_category_by_category_id($category_id);
        //echo'<pre>';print_r($data); exit();
        $data['content'] = $this->load->view('admin/edit_category',$data, true);
        $this->load->view('admin/master_admin', $data);
        
    }
    
    public function update_category() {
        $data = array();
        $category_id= $this->input->post('category_id', true);
        $data['category_name'] = $this->input->post('category_name', true);
        $data['category_description'] = $this->input->post('category_description', true);
        $data['publication_status'] = $this->input->post('publication_status', true);
        //echo'<pre>';        print_r($data);exit();
        $this->super_admin_model->update_category_by_category_id($data,$category_id);
        $sdata = array();
        $sdata['success'] = "Category is successfully Updated!!";
        $this->session->set_userdata($sdata);
        redirect('super_admin/edit_category/'.$category_id);
    }
    
    

    public function add_blog() {
        $data = array();
        $data['editor']=$this->data;
        $data['title'] = "Admin Panel";        
        $data['all_category'] = $this->super_admin_model->select_all_category();
        //echo'<pre>';print_r($data); exit();
        $data['content'] = $this->load->view('admin/add_blog',$data, true);
        $this->load->view('admin/master_admin', $data);
    }

    public function save_blog() {
        $data = array();
        //echo'<pre>';print_r($_POST); exit();
        //echo'<pre>';print_r($_FILES); exit();
        $config['upload_path'] = 'source/images/blog_images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '10000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $error =array();
        $fdata =array();
        $this->load->library('upload');
        $this->upload->initialize($config);
        //echo'<pre>';print_r($config); exit();

        if (!$this->upload->do_upload('blog_image')) {
            $error = $this->upload->display_errors();
        } else {
            $fdata =  $this->upload->data();
            $data['blog_image']=  base_url().$config['upload_path'].$fdata['file_name'];
           // echo'<pre>';print_r($fdata); exit();
            
        }

        $data['blog_title'] = $this->input->post('blog_title', true);
        $data['blog_short_description'] = $this->input->post('blog_short_description', true);
        $data['blog_long_description'] = $this->input->post('blog_long_description', true);
        $data['category_id'] = $this->input->post('category_id', true);
        $data['author_name'] = $this->input->post('author_name', true);
        $data['author_email'] = $this->input->post('author_email', true);
        $data['date'] = $this->input->post('date', true);
        $data['publication_status'] = $this->input->post('publication_status', true);
        //echo'<pre>';        print_r($data);exit();
        $this->general_model->save_info('tbl_blog', $data);

        $sdata = array();
        $sdata['success'] = "Blog is successfully Inserted!!";
        $this->session->set_userdata($sdata);
        redirect('super_admin/add_blog');
    }
    public function update_blog() {
        $data = array();
        //echo'<pre>';print_r($_POST); exit();
        //echo'<pre>';print_r($_FILES); exit();
        $config['upload_path'] = 'source/images/blog_images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '10000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $error =array();
        $fdata =array();
        $this->load->library('upload');
        $this->upload->initialize($config);
        //echo'<pre>';print_r($config); exit();

        if (!$this->upload->do_upload('blog_image')) {
            $error = $this->upload->display_errors();
        } else {
            $fdata =  $this->upload->data();
            $data['blog_image']=  base_url().$config['upload_path'].$fdata['file_name'];
           // echo'<pre>';print_r($fdata); exit();
            
        }

        $blog_id= $this->input->post('blog_id', true);
        $data['blog_title'] = $this->input->post('blog_title', true);
        $data['blog_short_description'] = $this->input->post('blog_short_description', true);
        $data['blog_long_description'] = $this->input->post('blog_long_description', true);
        $data['category_id'] = $this->input->post('category_id', true);
        $data['author_name'] = $this->input->post('author_name', true);
        $data['author_email'] = $this->input->post('author_email', true);
        $data['date'] = $this->input->post('date', true);
        $data['publication_status'] = $this->input->post('publication_status', true);
        //echo'<pre>';        print_r($data);exit();
        $this->super_admin_model->update_blog_by_blog_id($data,$blog_id);

        $sdata = array();
        $sdata['success'] = "Blog is successfully Updated!!";
        $this->session->set_userdata($sdata);
        redirect('super_admin/edit_blog/'.$blog_id);
    }
    
    public function show_blog() {
        $data = array();
        $data['title'] = "Admin Panel-Category";
        $data['all_blog'] = $this->super_admin_model->select_all_blog();
        $data['content'] = $this->load->view('admin/show_blog', $data, true);
        $this->load->view('admin/master_admin',$data);
    }

    public function delete_blog_by_blog_id($blog_id){
        $this->super_admin_model->delete_blog($blog_id);
        redirect('super_admin/show_blog');
    }
    
    public function edit_blog($blog_id){
        $data = array();
        $data['title'] = "Admin Panel-Edit Blog";        
        $data['all_category'] = $this->super_admin_model->select_all_category();
        
        $data['all_blog']=$this->super_admin_model->edit_blog_by_blog_id($blog_id);
        //echo'<pre>';print_r($data); exit();
        $data['content'] = $this->load->view('admin/edit_blog',$data, true);
        $this->load->view('admin/master_admin', $data);
        
    }
    
    public function unpublish_blog($blog_id){
        $this->super_admin_model->unpublished_blog_by_blog_id($blog_id);
        redirect('super_admin/show_blog');
    }
    public function publish_blog($blog_id){
        $this->super_admin_model->published_blog_by_blog_id($blog_id);
        redirect('super_admin/show_blog');
    }
    
}

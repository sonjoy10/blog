<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of super_admin_model
 *
 * @author sonjoy
 */
class Super_Admin_Model extends CI_Model{
    public function select_all_category(){
        $this->db->select('*');
        $this->db->from('tbl_category');
        $query=$this->db->get();
        $result=$query->result();
        return $result;
    }       
    
    public function delete_category($category_id){
        $this->db->where('category_id',$category_id);
        $this->db->delete('tbl_category');
    }
    
    public function unpublished_category_by_category_id($category_id){
        $this->db->set('publication_status',0);
        $this->db->where('category_id',$category_id);        
        $this->db->update('tbl_category');
    }
    public function published_category_by_category_id($category_id){
        $this->db->set('publication_status',1);
        $this->db->where('category_id',$category_id);        
        $this->db->update('tbl_category');
    }   
        
    public function select_all_blog(){
        $this->db->select('*');
        $this->db->from('tbl_blog');
        $query=$this->db->get();
        $result=$query->result();
        return $result;
    }
    public function edit_blog_by_blog_id($blog_id){
        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->where('blog_id',$blog_id);
        $query=$this->db->get();
        $result=$query->result();
        return $result;
    }
    public function edit_category_by_category_id($category_id){
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('category_id',$category_id);
        $query=$this->db->get();
        $result=$query->result();
        return $result;
    }
    
    public function delete_blog($blog_id){
        $this->db->where('blog_id',$blog_id);
        $this->db->delete('tbl_blog');
    }
    
    public function unpublished_blog_by_blog_id($blog_id){
        $this->db->set('publication_status',0);
        $this->db->where('blog_id',$blog_id);        
        $this->db->update('tbl_blog');
    }
    public function published_blog_by_blog_id($blog_id){
        $this->db->set('publication_status',1);
        $this->db->where('blog_id',$blog_id);        
        $this->db->update('tbl_blog');
    }
    
    public function update_blog_by_blog_id($data,$blog_id){        
        $this->db->where('blog_id',$blog_id);
        $this->db->update('tbl_blog',$data);
    }
    public function update_category_by_category_id($data,$category_id){        
        $this->db->where('category_id',$category_id);
        $this->db->update('tbl_category',$data);
    }
}
